package com.example.demo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.ModelRizal;
import com.example.demo.service.ApiService;

@RestController
@RequestMapping(value="/api/rizal")

public class ApiController {
	
	@Autowired
	
	private ApiService apiService;
	
	
	/*
	 * Post = melempar data 
	 * Get = mengambil Data 
	 * Put = mengupdate Data 
	 * Delete = menghapus Data
	 */
	

	@PostMapping(value="/post")
	@ResponseStatus(code = HttpStatus.CREATED)
	
	public Map<String, Object> postApi(@RequestBody ModelRizal modelRizal){
		this.apiService.create(modelRizal);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Berhasil", Boolean.TRUE);
		map.put("Notifikasi", "Data Tersimpan");
	
		return map;
	}
	

	@GetMapping(value="/get")
	@ResponseStatus(code = HttpStatus.OK)
    public List<ModelRizal> getApi() {
    List<ModelRizal> modelRizalList = new ArrayList<ModelRizal>(); 
    modelRizalList = this.apiService.lihat();
    return modelRizalList;
   }
   
	
	
	@PutMapping("/put")
	@ResponseStatus(code = HttpStatus.CREATED)
      public Map<String, Object> updateApi (@RequestBody ModelRizal modelRizal){
	  this.apiService.update(modelRizal);
	  Map<String, Object> map = new HashMap<String, Object>();
	  map.put("Berhasil", Boolean.TRUE);
	  map.put("Success", "berhasil ditambah");
	  
	  return map;
  }
	
	
	@DeleteMapping(value="/delete/{namaRizal}")
	@ResponseStatus(code = HttpStatus.GONE)
	
	public Map<String, Object> deleteApi (@RequestBody ModelRizal modelRizal){
		this.apiService.delete(modelRizal);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Berhasil", Boolean.TRUE);
		map.put("Dihapus", "data Berhasil di hapus");
		
		return map;
	}
	
	
}
