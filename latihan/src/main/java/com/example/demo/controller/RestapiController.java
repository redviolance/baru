package com.example.demo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.ModelApi;
import com.example.demo.service.RestapiService;

@RestController
@RequestMapping(value="rest/api")

public class RestapiController {
	
	@Autowired
	private RestapiService restapiService;

	
	//buat ngepost
	
	
	@PostMapping(value="/post") // Api memakai POST mapping untuk melempar data
	@ResponseStatus(code = HttpStatus.CREATED)
	public Map<String, Object> postApi (@RequestBody ModelApi modelApi){
		this.restapiService.tambah(modelApi);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Berhasil", Boolean.TRUE);
		map.put("Success", "data Berhasil ditambah");
		
		return map;
	} 
	
	
	
	@GetMapping(value="/get")
	@ResponseStatus(code = HttpStatus.OK)
   public List<ModelApi> getApi(@RequestBody ModelApi modelApi){
	   List<ModelApi> modelApiList = new ArrayList<ModelApi>();
	   modelApiList = this.restapiService.lihat();
	   return modelApiList;
   }
	
	
	@GetMapping(value="/dibawah")
	@ResponseStatus(code = HttpStatus.OK)
   public List<ModelApi> usiaDibawah(@RequestBody ModelApi modelApi){
	   List<ModelApi> modelApiList = new ArrayList<ModelApi>();
	   modelApiList = this.restapiService.less();
	   return modelApiList;
   }

	
	
	
	
	@PostMapping(value="/put") // Api memakai POST mapping untuk melempar data
	@ResponseStatus(code = HttpStatus.CREATED)
	public Map<String, Object> putApi (@RequestBody ModelApi modelApi){
		this.restapiService.update(modelApi);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Berhasil", Boolean.TRUE);
		map.put("Success", "data Berhasil dirubah");
		
		return map;
	} 
	
	
	@DeleteMapping(value="/delete/{namaApi}")
	@ResponseStatus(code = HttpStatus.GONE)
 	public Map<String, Object> deleteApi(@PathVariable ModelApi modelApi){
		this.restapiService.delete(modelApi);// masih belum
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Berhasil", Boolean.TRUE);
		map.put("Berhasil", "Data Berhasil Dihapus");
		
		return map;
	}
	
	
}
