package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.ModelRizal;
import com.example.demo.service.RizalService;

@Controller
public class RizalController {
	
	
	
	@Autowired
	private RizalService rizalService; 
	
	
	
	
	@RequestMapping(value="tambah_data")
	public String TambahRizal() {
	  String html ="rizal/tambah_rizal";
		return html;
	}
	
	@RequestMapping(value="hasil_tambah_rizal")
	public String HasilTambahRizal(HttpServletRequest request, Model model) {
		
		String namaRizal = request.getParameter("namaRizal");
		int umurRizal = Integer.valueOf(request.getParameter("umurRizal")) ;
		
		ModelRizal modelRizal = new ModelRizal();
		
		modelRizal.setNamaRizal(namaRizal);
		modelRizal.setUmurRizal(umurRizal);
		
		
		rizalService.Tambah(modelRizal);
		

		List<ModelRizal> modelRizalList = new ArrayList<ModelRizal>();
		modelRizalList = this.rizalService.Lihat();
		
		model.addAttribute("ModelRizalList", modelRizalList);
		String html = "rizal/hasil_tambah";
		return html;
	}
	
	@RequestMapping(value="asc")
	public String hasilAsc(Model model) {
		List<ModelRizal> modelRizalList = new ArrayList<ModelRizal>();
		modelRizalList = this.rizalService.asc();
		
		model.addAttribute("ModelRizalList", modelRizalList);
		String html ="rizal/hasil_asc"; 
		return html;
	}
	
	@RequestMapping(value="cari_nama")
	public String cariNama(HttpServletRequest request,Model model) {
		String namaRizal = request.getParameter("cari_nama");
		
		List<ModelRizal> modelRizalList = new ArrayList<ModelRizal>();
		modelRizalList = this.rizalService.cariNama(namaRizal);
		
		model.addAttribute("ModelRizalList", modelRizalList);
		String html ="rizal/hasil_tambah"; 
		return html;
	}
	
	
	
	

}
