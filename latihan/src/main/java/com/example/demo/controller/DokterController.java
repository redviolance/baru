package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.ModelDokter;
import com.example.demo.service.DokterService;

@Controller
public class DokterController {

	@Autowired
	private DokterService dokterService;
	
	@RequestMapping(value="tambah_dokter")
	public String TambahDokter() {
		String html= "dokter/tambah_dokter"; 
        return html;	
	}
	
	
	@RequestMapping(value="hasil_tambah_dokter")
	
	public String HasilTambahDokter(HttpServletRequest request, Model model) {
		
		String idDokter = request.getParameter("idDokter");
		String namaDokter = request.getParameter("namaDokter");
		String gelarDokter = request.getParameter("gelarDokter");
		
		
		ModelDokter modelDokter = new ModelDokter();
		
		modelDokter.setGelarDokter(gelarDokter);
		modelDokter.setIdDokter(idDokter);
		modelDokter.setNamaDokter(namaDokter);
		
		dokterService.tambah(modelDokter);
		
		List<ModelDokter> modelDokterList = new ArrayList<ModelDokter>();
		modelDokterList = this.dokterService.lihat();
		model.addAttribute("DokterModelList", modelDokterList);
		
		
		
		String html= "dokter/hasil_tambah"; 
        return html;	
	}
	
	
	@RequestMapping(value="banyak_dokter")
	public String hasilBanyak(Model model) {
		
		List<ModelDokter> modelDokterList = new ArrayList<ModelDokter>();
		modelDokterList = this.dokterService.lihat();
		model.addAttribute("DokterModelList", modelDokterList);
		
		
		
		String html= "dokter/list_dokter"; 
        return html;	
	}
		
	
	
	
}
