package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.ModelKartu;
import com.example.demo.service.KartuService;

@Controller
public class KartuController {

	
	@Autowired
	private KartuService kartuService;
	
	@RequestMapping(value="tambah_kartu")
	public String menuTambahKartu() {
		String html="kartu/tambah_kartu";
	    return html;
	}
	
	
	
	@RequestMapping(value="hasil_tambah_kartu")
	public String menuHasilTambahKartu(HttpServletRequest request,Model model) {
		
		String namaKartu = request.getParameter("namaKartu");
		int kodeKartu = Integer.valueOf(request.getParameter("kodeKartu"));
		String jenisKartu = request.getParameter("jenisKartu");
		
		ModelKartu modelKartu = new ModelKartu();
		
		modelKartu.setJenisKartu(jenisKartu);
		modelKartu.setKodeKartu(kodeKartu);
		modelKartu.setNamaKartu(namaKartu);
		
		
		kartuService.Tambah(modelKartu);
		
		List<ModelKartu> modelKartuList = new ArrayList<ModelKartu>();
		modelKartuList = kartuService.read();
		model.addAttribute("ModelKartuList", modelKartuList);
		
		
		String html="kartu/hasil_tambah_kartu";
	    return html;
	}
	
	
	
	
	
	
	
	
	
	
	
	//hasil_tambah_kartu
}
