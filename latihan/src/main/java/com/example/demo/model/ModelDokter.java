package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_DOKTER")

public class ModelDokter {

	@Id
	@Column(name="COL_ID_DOKTER")
	private String idDokter;
	
	@Column(name="COL_NAMA_DOKTER")
	private String namaDokter;
	
	@Column(name="COL_GELAR_DOKTER")
	private String gelarDokter;

	
	
	public String getIdDokter() {
		return idDokter;
	}

	public void setIdDokter(String idDokter) {
		this.idDokter = idDokter;
	}

	public String getNamaDokter() {
		return namaDokter;
	}

	public void setNamaDokter(String namaDokter) {
		this.namaDokter = namaDokter;
	}

	public String getGelarDokter() {
		return gelarDokter;
	}

	public void setGelarDokter(String gelarDokter) {
		this.gelarDokter = gelarDokter;
	}
	
	
	
	
	
	
}
