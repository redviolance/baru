package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_YUGIOH")
public class ModelKartu {
	
	@Id
	@Column(name="COL_NAMA")
	private String namaKartu;
	
	
	@Column(name="COL_KODE")
	private int kodeKartu;
	
	@Column(name="COL_JENIS")
	private String jenisKartu;
	
	
	public String getNamaKartu() {
		return namaKartu;
	}
	public void setNamaKartu(String namaKartu) {
		this.namaKartu = namaKartu;
	}
	public int getKodeKartu() {
		return kodeKartu;
	}
	public void setKodeKartu(int kodeKartu) {
		this.kodeKartu = kodeKartu;
	}
	public String getJenisKartu() {
		return jenisKartu;
	}
	public void setJenisKartu(String jenisKartu) {
		this.jenisKartu = jenisKartu;
	}
	
	
}
