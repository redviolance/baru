package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_RIZAL")

public class ModelRizal {


	@Id
	@Column(name="CLM_NAMA_RIZAL")
	private String namaRizal;
	
	
	@Column(name="CLM_UMUR_RIZAL")
    private int umurRizal;



public String getNamaRizal() {
	return namaRizal;
}
public void setNamaRizal(String namaRizal) {
	this.namaRizal = namaRizal;
}
public int getUmurRizal() {
	return umurRizal;
}
public void setUmurRizal(int umurRizal) {
	this.umurRizal = umurRizal;
}


	
	
	
}
