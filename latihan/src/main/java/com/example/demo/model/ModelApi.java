package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_API")

public class ModelApi {

	
	@Id
	@Column(name="CLM_NAMA")
	private String namaApi;
	
	@Column(name="CLM_USIA")
	private int usiaApi;
	
	
	public String getNamaApi() {
		return namaApi;
	}
	public void setNamaApi(String namaApi) {
		this.namaApi = namaApi;
	}
	public int getUsiaApi() {
		return usiaApi;
	}
	public void setUsiaApi(int usiaApi) {
		this.usiaApi = usiaApi;
	}
	
	
	
	
}
