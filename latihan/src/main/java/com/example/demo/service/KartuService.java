package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.ModelKartu;
import com.example.demo.repository.KartuRepository;

@Transactional
@Service

public class KartuService {

	@Autowired
	private KartuRepository kartuRepository;
	
	
	public void Tambah(ModelKartu modelKartu) {
		kartuRepository.save(modelKartu);		
	}
	
	public List<ModelKartu>read(){
		return kartuRepository.findAll();
	}
	
}
