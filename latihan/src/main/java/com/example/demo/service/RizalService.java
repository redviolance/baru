package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.ModelRizal;
import com.example.demo.repository.RizalRepository;

@Transactional
@Service

public class RizalService {

	
	@Autowired
	private RizalRepository rizalRepository;
	
	
	public void Tambah(ModelRizal modelRizal) {
		rizalRepository.save(modelRizal);
	}

	
	public List<ModelRizal> Lihat(){
		return rizalRepository.findAll();
		
	}
	
	public List<ModelRizal> asc(){
		return rizalRepository.urutin();
	}
	 
	public List<ModelRizal> cariNama(String namaRizal){
		return rizalRepository.nyariNamayang(namaRizal);
	}
}
