package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.ModelDokter;
import com.example.demo.repository.DokterRepository;

@Transactional
@Service

public class DokterService {

	@Autowired
	private DokterRepository dokterRepository;
	
	public void tambah(ModelDokter modelDokter) {
		dokterRepository.save(modelDokter);
		
	}
	
	public List<ModelDokter> lihat(){
		return dokterRepository.findAll();
	}
	
}
