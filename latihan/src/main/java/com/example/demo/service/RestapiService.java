package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.ModelApi;
import com.example.demo.repository.RestapiRepository;

@Transactional
@Service


public class RestapiService {


	@Autowired
	private RestapiRepository restapiRepository;
	
	
	public void tambah(ModelApi modelApi) {
	  restapiRepository.save(modelApi);	
	}
	
	public void update(ModelApi  modelApi) {
		restapiRepository.save(modelApi);
	}

	public void delete(ModelApi modelApi) {
		restapiRepository.delete(modelApi);
	}
	
	
	
	public List<ModelApi> lihat(){
		return restapiRepository.findAll();
	}
	
	
	public List<ModelApi> less(){
		return restapiRepository.kurangDari();
	}
}
