package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.ModelRizal;
import com.example.demo.repository.ApiRepository;

@Transactional
@Service

public class ApiService {

	@Autowired 
	private ApiRepository apiRepository;
	
	
	public void create(ModelRizal modelRizal) {
		apiRepository.save(modelRizal);
	}
	
	public void update(ModelRizal modelRizal) {
		apiRepository.save(modelRizal);
	} 
	
	
	public void delete(ModelRizal modelRizal) {
		apiRepository.delete(modelRizal);
	}
	
	public List<ModelRizal> lihat(){
		return apiRepository.findAll();
	}
	
}
