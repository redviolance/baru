package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.ModelRizal;

public interface RizalRepository extends JpaRepository<ModelRizal, String> {

	@Query("SELECT F FROM ModelRizal F ORDER BY F.namaRizal ASC")
	List<ModelRizal> urutin();
	
	@Query("SELECT F FROM ModelRizal F where F.namaRizal LIKE %?1%")
	List<ModelRizal> nyariNamayang(String namaRizal);

	
}
