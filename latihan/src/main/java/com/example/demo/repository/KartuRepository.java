package com.example.demo.repository;



import org.springframework.data.jpa.repository.JpaRepository;


import com.example.demo.model.ModelKartu;

public interface KartuRepository extends JpaRepository<ModelKartu, String> {
	
	
}
