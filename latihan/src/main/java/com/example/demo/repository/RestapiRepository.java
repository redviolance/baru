package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.ModelApi;

public interface RestapiRepository extends JpaRepository<ModelApi, String>{

	@Query("SELECT F FROM ModelApi F where F.usiaApi > 5")
	List<ModelApi> kurangDari();
	
	
}
