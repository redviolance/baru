package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.ModelDokter;

public interface DokterRepository extends JpaRepository<ModelDokter, String> {

}
